UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 3 AND Name = 'Cargo Hold - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 3 AND Name = 'Cargo Hold';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cargo Hold (', 'Cargo Hold₂ (') WHERE SDComponentID = 3 AND Name LIKE 'Cargo Hold (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 8 AND Name = 'Crew Quarters';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 8 AND Name = 'Crew Quarters';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Crew Quarters (', 'Crew Quarters₄ (') WHERE SDComponentID = 8 AND Name LIKE 'Crew Quarters (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 479 AND Name = 'Cryogenic Transport';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 479 AND Name = 'Cryogenic Transport';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cryogenic Transport (', 'Cryogenic Transport₃ (') WHERE SDComponentID = 479 AND Name LIKE 'Cryogenic Transport (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 600 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 600 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₄ (') WHERE SDComponentID = 600 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 728 AND Name = 'Troop Transport Bay - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 728 AND Name = 'Troop Transport Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Bay (', 'Troop Transport Bay₄ (') WHERE SDComponentID = 728 AND Name LIKE 'Troop Transport Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 12017 AND Name = 'ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12017 AND Name = 'ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-1 (', 'ECM-1₁ (') WHERE SDComponentID = 12017 AND Name LIKE 'ECM-1 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 12018 AND Name = 'ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12018 AND Name = 'ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-2 (', 'ECM-2₁ (') WHERE SDComponentID = 12018 AND Name LIKE 'ECM-2 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 12019 AND Name = 'ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12019 AND Name = 'ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-3 (', 'ECM-3₁ (') WHERE SDComponentID = 12019 AND Name LIKE 'ECM-3 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 12020 AND Name = 'ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12020 AND Name = 'ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-4 (', 'ECM-4₁ (') WHERE SDComponentID = 12020 AND Name LIKE 'ECM-4 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 12021 AND Name = 'ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12021 AND Name = 'ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-5 (', 'ECM-5₁ (') WHERE SDComponentID = 12021 AND Name LIKE 'ECM-5 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-6' WHERE SDComponentID = 12022 AND Name = 'ECM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12022 AND Name = 'ECM-6';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-6 (', 'ECM-6₁ (') WHERE SDComponentID = 12022 AND Name LIKE 'ECM-6 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-7' WHERE SDComponentID = 12023 AND Name = 'ECM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12023 AND Name = 'ECM-7';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-7 (', 'ECM-7₁ (') WHERE SDComponentID = 12023 AND Name LIKE 'ECM-7 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-8' WHERE SDComponentID = 12024 AND Name = 'ECM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12024 AND Name = 'ECM-8';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-8 (', 'ECM-8₁ (') WHERE SDComponentID = 12024 AND Name LIKE 'ECM-8 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-9' WHERE SDComponentID = 12025 AND Name = 'ECM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12025 AND Name = 'ECM-9';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-9 (', 'ECM-9₁ (') WHERE SDComponentID = 12025 AND Name LIKE 'ECM-9 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-10' WHERE SDComponentID = 12026 AND Name = 'ECM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12026 AND Name = 'ECM-10';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-10 (', 'ECM-10₁ (') WHERE SDComponentID = 12026 AND Name LIKE 'ECM-10 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 12027 AND Name = 'ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12027 AND Name = 'ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-1 (', 'ECCM-1₁ (') WHERE SDComponentID = 12027 AND Name LIKE 'ECCM-1 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 12028 AND Name = 'ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12028 AND Name = 'ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-2 (', 'ECCM-2₁ (') WHERE SDComponentID = 12028 AND Name LIKE 'ECCM-2 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 12029 AND Name = 'ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12029 AND Name = 'ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-3 (', 'ECCM-3₁ (') WHERE SDComponentID = 12029 AND Name LIKE 'ECCM-3 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 12030 AND Name = 'ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12030 AND Name = 'ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-4 (', 'ECCM-4₁ (') WHERE SDComponentID = 12030 AND Name LIKE 'ECCM-4 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 12031 AND Name = 'ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12031 AND Name = 'ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-5 (', 'ECCM-5₁ (') WHERE SDComponentID = 12031 AND Name LIKE 'ECCM-5 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-6' WHERE SDComponentID = 12032 AND Name = 'ECCM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12032 AND Name = 'ECCM-6';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-6 (', 'ECCM-6₁ (') WHERE SDComponentID = 12032 AND Name LIKE 'ECCM-6 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-7' WHERE SDComponentID = 12033 AND Name = 'ECCM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12033 AND Name = 'ECCM-7';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-7 (', 'ECCM-7₁ (') WHERE SDComponentID = 12033 AND Name LIKE 'ECCM-7 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-8' WHERE SDComponentID = 12034 AND Name = 'ECCM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12034 AND Name = 'ECCM-8';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-8 (', 'ECCM-8₁ (') WHERE SDComponentID = 12034 AND Name LIKE 'ECCM-8 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-9' WHERE SDComponentID = 12035 AND Name = 'ECCM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12035 AND Name = 'ECCM-9';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-9 (', 'ECCM-9₁ (') WHERE SDComponentID = 12035 AND Name LIKE 'ECCM-9 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-10' WHERE SDComponentID = 12036 AND Name = 'ECCM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 12036 AND Name = 'ECCM-10';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-10 (', 'ECCM-10₁ (') WHERE SDComponentID = 12036 AND Name LIKE 'ECCM-10 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 12037 AND Name = 'Compact ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12037 AND Name = 'ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-1 (', 'ECM-1₂ (') WHERE SDComponentID = 12037 AND Name LIKE 'ECM-1 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 12038 AND Name = 'Compact ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12038 AND Name = 'ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-2 (', 'ECM-2₂ (') WHERE SDComponentID = 12038 AND Name LIKE 'ECM-2 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 12039 AND Name = 'Compact ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12039 AND Name = 'ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-3 (', 'ECM-3₂ (') WHERE SDComponentID = 12039 AND Name LIKE 'ECM-3 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 12040 AND Name = 'Compact ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12040 AND Name = 'ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-4 (', 'ECM-4₂ (') WHERE SDComponentID = 12040 AND Name LIKE 'ECM-4 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 12041 AND Name = 'Compact ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12041 AND Name = 'ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-5 (', 'ECM-5₂ (') WHERE SDComponentID = 12041 AND Name LIKE 'ECM-5 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-6' WHERE SDComponentID = 12042 AND Name = 'Compact ECM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12042 AND Name = 'ECM-6';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-6 (', 'ECM-6₂ (') WHERE SDComponentID = 12042 AND Name LIKE 'ECM-6 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-7' WHERE SDComponentID = 12043 AND Name = 'Compact ECM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12043 AND Name = 'ECM-7';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-7 (', 'ECM-7₂ (') WHERE SDComponentID = 12043 AND Name LIKE 'ECM-7 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-8' WHERE SDComponentID = 12044 AND Name = 'Compact ECM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12044 AND Name = 'ECM-8';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-8 (', 'ECM-8₂ (') WHERE SDComponentID = 12044 AND Name LIKE 'ECM-8 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-9' WHERE SDComponentID = 12045 AND Name = 'Compact ECM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12045 AND Name = 'ECM-9';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-9 (', 'ECM-9₂ (') WHERE SDComponentID = 12045 AND Name LIKE 'ECM-9 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-10' WHERE SDComponentID = 12046 AND Name = 'Compact ECM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12046 AND Name = 'ECM-10';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-10 (', 'ECM-10₂ (') WHERE SDComponentID = 12046 AND Name LIKE 'ECM-10 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 12047 AND Name = 'Compact ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12047 AND Name = 'ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-1 (', 'ECCM-1₂ (') WHERE SDComponentID = 12047 AND Name LIKE 'ECCM-1 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 12048 AND Name = 'Compact ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12048 AND Name = 'ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-2 (', 'ECCM-2₂ (') WHERE SDComponentID = 12048 AND Name LIKE 'ECCM-2 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 12049 AND Name = 'Compact ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12049 AND Name = 'ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-3 (', 'ECCM-3₂ (') WHERE SDComponentID = 12049 AND Name LIKE 'ECCM-3 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 12050 AND Name = 'Compact ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12050 AND Name = 'ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-4 (', 'ECCM-4₂ (') WHERE SDComponentID = 12050 AND Name LIKE 'ECCM-4 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 12051 AND Name = 'Compact ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12051 AND Name = 'ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-5 (', 'ECCM-5₂ (') WHERE SDComponentID = 12051 AND Name LIKE 'ECCM-5 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-6' WHERE SDComponentID = 12052 AND Name = 'Compact ECCM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12052 AND Name = 'ECCM-6';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-6 (', 'ECCM-6₂ (') WHERE SDComponentID = 12052 AND Name LIKE 'ECCM-6 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-7' WHERE SDComponentID = 12053 AND Name = 'Compact ECCM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12053 AND Name = 'ECCM-7';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-7 (', 'ECCM-7₂ (') WHERE SDComponentID = 12053 AND Name LIKE 'ECCM-7 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-8' WHERE SDComponentID = 12054 AND Name = 'Compact ECCM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12054 AND Name = 'ECCM-8';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-8 (', 'ECCM-8₂ (') WHERE SDComponentID = 12054 AND Name LIKE 'ECCM-8 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-9' WHERE SDComponentID = 12055 AND Name = 'Compact ECCM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12055 AND Name = 'ECCM-9';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-9 (', 'ECCM-9₂ (') WHERE SDComponentID = 12055 AND Name LIKE 'ECCM-9 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-10' WHERE SDComponentID = 12056 AND Name = 'Compact ECCM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 12056 AND Name = 'ECCM-10';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-10 (', 'ECCM-10₂ (') WHERE SDComponentID = 12056 AND Name LIKE 'ECCM-10 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 180days' WHERE SDComponentID = 24990 AND Name = 'Jump Point Stabilisation Module - 180';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 24990 AND Name = 'Jump Point Stabilisation Module 180days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 180days (', 'Jump Point Stabilisation Module 180days₂ (') WHERE SDComponentID = 24990 AND Name LIKE 'Jump Point Stabilisation Module 180days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 150days' WHERE SDComponentID = 24993 AND Name = 'Jump Point Stabilisation Module - 150';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 24993 AND Name = 'Jump Point Stabilisation Module 150days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 150days (', 'Jump Point Stabilisation Module 150days₃ (') WHERE SDComponentID = 24993 AND Name LIKE 'Jump Point Stabilisation Module 150days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 120days' WHERE SDComponentID = 24994 AND Name = 'Jump Point Stabilisation Module - 120';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 24994 AND Name = 'Jump Point Stabilisation Module 120days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 120days (', 'Jump Point Stabilisation Module 120days₄ (') WHERE SDComponentID = 24994 AND Name LIKE 'Jump Point Stabilisation Module 120days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 90days' WHERE SDComponentID = 24995 AND Name = 'Jump Point Stabilisation Module - 90';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₅' WHERE SDComponentID = 24995 AND Name = 'Jump Point Stabilisation Module 90days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 90days (', 'Jump Point Stabilisation Module 90days₅ (') WHERE SDComponentID = 24995 AND Name LIKE 'Jump Point Stabilisation Module 90days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 60days' WHERE SDComponentID = 24996 AND Name = 'Jump Point Stabilisation Module - 60';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₆' WHERE SDComponentID = 24996 AND Name = 'Jump Point Stabilisation Module 60days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 60days (', 'Jump Point Stabilisation Module 60days₆ (') WHERE SDComponentID = 24996 AND Name LIKE 'Jump Point Stabilisation Module 60days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 45days' WHERE SDComponentID = 24997 AND Name = 'Jump Point Stabilisation Module - 45';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₇' WHERE SDComponentID = 24997 AND Name = 'Jump Point Stabilisation Module 45days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 45days (', 'Jump Point Stabilisation Module 45days₇ (') WHERE SDComponentID = 24997 AND Name LIKE 'Jump Point Stabilisation Module 45days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 30days' WHERE SDComponentID = 24998 AND Name = 'Jump Point Stabilisation Module - 30';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₈' WHERE SDComponentID = 24998 AND Name = 'Jump Point Stabilisation Module 30days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 30days (', 'Jump Point Stabilisation Module 30days₈ (') WHERE SDComponentID = 24998 AND Name LIKE 'Jump Point Stabilisation Module 30days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 20days' WHERE SDComponentID = 24999 AND Name = 'Jump Point Stabilisation Module - 20';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₉' WHERE SDComponentID = 24999 AND Name = 'Jump Point Stabilisation Module 20days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 20days (', 'Jump Point Stabilisation Module 20days₉ (') WHERE SDComponentID = 24999 AND Name LIKE 'Jump Point Stabilisation Module 20days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 25147 AND Name = 'Engineering Spaces';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 25147 AND Name = 'Engineering Spaces';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Engineering Spaces (', 'Engineering Spaces₄ (') WHERE SDComponentID = 25147 AND Name LIKE 'Engineering Spaces (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 26265 AND Name = 'Crew Quarters - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26265 AND Name = 'Crew Quarters';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Crew Quarters (', 'Crew Quarters₃ (') WHERE SDComponentID = 26265 AND Name LIKE 'Crew Quarters (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 26266 AND Name = 'Fuel Storage - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26266 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₃ (') WHERE SDComponentID = 26266 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 26267 AND Name = 'Engineering Spaces - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26267 AND Name = 'Engineering Spaces';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Engineering Spaces (', 'Engineering Spaces₃ (') WHERE SDComponentID = 26267 AND Name LIKE 'Engineering Spaces (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 26354 AND Name = 'Small Craft ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26354 AND Name = 'ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-5 (', 'ECM-5₃ (') WHERE SDComponentID = 26354 AND Name LIKE 'ECM-5 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 26355 AND Name = 'Small Craft ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26355 AND Name = 'ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-1 (', 'ECM-1₃ (') WHERE SDComponentID = 26355 AND Name LIKE 'ECM-1 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 26357 AND Name = 'Small Craft ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26357 AND Name = 'ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-2 (', 'ECM-2₃ (') WHERE SDComponentID = 26357 AND Name LIKE 'ECM-2 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 26359 AND Name = 'Small Craft ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26359 AND Name = 'ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-3 (', 'ECM-3₃ (') WHERE SDComponentID = 26359 AND Name LIKE 'ECM-3 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 26361 AND Name = 'Small Craft ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26361 AND Name = 'ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECM-4 (', 'ECM-4₃ (') WHERE SDComponentID = 26361 AND Name LIKE 'ECM-4 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 26363 AND Name = 'Small Craft ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26363 AND Name = 'ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-5 (', 'ECCM-5₃ (') WHERE SDComponentID = 26363 AND Name LIKE 'ECCM-5 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 26364 AND Name = 'Small Craft ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26364 AND Name = 'ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-1 (', 'ECCM-1₃ (') WHERE SDComponentID = 26364 AND Name LIKE 'ECCM-1 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 26365 AND Name = 'Small Craft ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26365 AND Name = 'ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-2 (', 'ECCM-2₃ (') WHERE SDComponentID = 26365 AND Name LIKE 'ECCM-2 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 26366 AND Name = 'Small Craft ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26366 AND Name = 'ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-3 (', 'ECCM-3₃ (') WHERE SDComponentID = 26366 AND Name LIKE 'ECCM-3 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 26367 AND Name = 'Small Craft ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 26367 AND Name = 'ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'ECCM-4 (', 'ECCM-4₃ (') WHERE SDComponentID = 26367 AND Name LIKE 'ECCM-4 (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 26420 AND Name = 'Compressed Fuel Storage System';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 26420 AND Name = 'Compressed Fuel Storage System';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Compressed Fuel Storage System (', 'Compressed Fuel Storage System₂ (') WHERE SDComponentID = 26420 AND Name LIKE 'Compressed Fuel Storage System (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 27132 AND Name = 'Large Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₅' WHERE SDComponentID = 27132 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Maintenance Storage Bay (', 'Maintenance Storage Bay₅ (') WHERE SDComponentID = 27132 AND Name LIKE 'Maintenance Storage Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 27133 AND Name = 'Engineering Spaces - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 27133 AND Name = 'Engineering Spaces';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Engineering Spaces (', 'Engineering Spaces₂ (') WHERE SDComponentID = 27133 AND Name LIKE 'Engineering Spaces (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 27134 AND Name = 'Engineering Spaces - Fighter';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 27134 AND Name = 'Engineering Spaces';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Engineering Spaces (', 'Engineering Spaces₁ (') WHERE SDComponentID = 27134 AND Name LIKE 'Engineering Spaces (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 360days' WHERE SDComponentID = 33215 AND Name = 'Small Jump Point Stabilisation Module';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 33215 AND Name = 'Jump Point Stabilisation Module 360days';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Jump Point Stabilisation Module 360days (', 'Jump Point Stabilisation Module 360days₁ (') WHERE SDComponentID = 33215 AND Name LIKE 'Jump Point Stabilisation Module 360days (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 33426 AND Name = 'Troop Transport Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 33426 AND Name = 'Troop Transport Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Bay (', 'Troop Transport Bay₃ (') WHERE SDComponentID = 33426 AND Name LIKE 'Troop Transport Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 33433 AND Name = 'Boat Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 33433 AND Name = 'Boat Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Boat Bay (', 'Boat Bay₂ (') WHERE SDComponentID = 33433 AND Name LIKE 'Boat Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 38117 AND Name = 'Fuel Storage - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 38117 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₂ (') WHERE SDComponentID = 38117 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 43528 AND Name = 'Cargo Hold - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 43528 AND Name = 'Cargo Hold';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cargo Hold (', 'Cargo Hold₃ (') WHERE SDComponentID = 43528 AND Name LIKE 'Cargo Hold (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 43529 AND Name = 'Fuel Storage - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₅' WHERE SDComponentID = 43529 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₅ (') WHERE SDComponentID = 43529 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 43530 AND Name = 'Fuel Storage - Ultra Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₇' WHERE SDComponentID = 43530 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₇ (') WHERE SDComponentID = 43530 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 43531 AND Name = 'Fuel Storage - Very Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₆' WHERE SDComponentID = 43531 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₆ (') WHERE SDComponentID = 43531 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 43532 AND Name = 'Cryogenic Transport - Emergency';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 43532 AND Name = 'Cryogenic Transport';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cryogenic Transport (', 'Cryogenic Transport₁ (') WHERE SDComponentID = 43532 AND Name LIKE 'Cryogenic Transport (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 43533 AND Name = 'Cryogenic Transport - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 43533 AND Name = 'Cryogenic Transport';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cryogenic Transport (', 'Cryogenic Transport₂ (') WHERE SDComponentID = 43533 AND Name LIKE 'Cryogenic Transport (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 47485 AND Name = 'Crew Quarters - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 47485 AND Name = 'Crew Quarters';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Crew Quarters (', 'Crew Quarters₂ (') WHERE SDComponentID = 47485 AND Name LIKE 'Crew Quarters (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 55437 AND Name = 'Troop Transport Drop Bay - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 55437 AND Name = 'Troop Transport Drop Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Drop Bay (', 'Troop Transport Drop Bay₄ (') WHERE SDComponentID = 55437 AND Name LIKE 'Troop Transport Drop Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 55438 AND Name = 'Troop Transport Drop Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 55438 AND Name = 'Troop Transport Drop Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Drop Bay (', 'Troop Transport Drop Bay₃ (') WHERE SDComponentID = 55438 AND Name LIKE 'Troop Transport Drop Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 62453 AND Name = 'Crew Quarters - Fighter';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 62453 AND Name = 'Crew Quarters';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Crew Quarters (', 'Crew Quarters₁ (') WHERE SDComponentID = 62453 AND Name LIKE 'Crew Quarters (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 62489 AND Name = 'Boat Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 62489 AND Name = 'Boat Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Boat Bay (', 'Boat Bay₁ (') WHERE SDComponentID = 62489 AND Name LIKE 'Boat Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 64796 AND Name = 'Compressed Fuel Storage System - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 64796 AND Name = 'Compressed Fuel Storage System';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Compressed Fuel Storage System (', 'Compressed Fuel Storage System₃ (') WHERE SDComponentID = 64796 AND Name LIKE 'Compressed Fuel Storage System (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 64797 AND Name = 'Compressed Fuel Storage System - Very Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 64797 AND Name = 'Compressed Fuel Storage System';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Compressed Fuel Storage System (', 'Compressed Fuel Storage System₄ (') WHERE SDComponentID = 64797 AND Name LIKE 'Compressed Fuel Storage System (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 65061 AND Name = 'Compressed Fuel Storage System - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 65061 AND Name = 'Compressed Fuel Storage System';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Compressed Fuel Storage System (', 'Compressed Fuel Storage System₁ (') WHERE SDComponentID = 65061 AND Name LIKE 'Compressed Fuel Storage System (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 65307 AND Name = 'Cargo Hold - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 65307 AND Name = 'Cargo Hold';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cargo Hold (', 'Cargo Hold₁ (') WHERE SDComponentID = 65307 AND Name LIKE 'Cargo Hold (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 65454 AND Name = 'Troop Transport Boarding Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 65454 AND Name = 'Troop Transport Boarding Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Boarding Bay (', 'Troop Transport Boarding Bay₃ (') WHERE SDComponentID = 65454 AND Name LIKE 'Troop Transport Boarding Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 65848 AND Name = 'Troop Transport Boarding Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 65848 AND Name = 'Troop Transport Boarding Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Boarding Bay (', 'Troop Transport Boarding Bay₂ (') WHERE SDComponentID = 65848 AND Name LIKE 'Troop Transport Boarding Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 65849 AND Name = 'Troop Transport Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 65849 AND Name = 'Troop Transport Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Bay (', 'Troop Transport Bay₁ (') WHERE SDComponentID = 65849 AND Name LIKE 'Troop Transport Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 65850 AND Name = 'Troop Transport Drop Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 65850 AND Name = 'Troop Transport Drop Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Drop Bay (', 'Troop Transport Drop Bay₁ (') WHERE SDComponentID = 65850 AND Name LIKE 'Troop Transport Drop Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 67058 AND Name = 'Fuel Storage - Fighter';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 67058 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Fuel Storage (', 'Fuel Storage₁ (') WHERE SDComponentID = 67058 AND Name LIKE 'Fuel Storage (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 67059 AND Name = 'Cargo Hold - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 67059 AND Name = 'Cargo Hold';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Cargo Hold (', 'Cargo Hold₄ (') WHERE SDComponentID = 67059 AND Name LIKE 'Cargo Hold (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 67060 AND Name = 'Troop Transport Boarding Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 67060 AND Name = 'Troop Transport Boarding Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Boarding Bay (', 'Troop Transport Boarding Bay₁ (') WHERE SDComponentID = 67060 AND Name LIKE 'Troop Transport Boarding Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76178 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₄' WHERE SDComponentID = 76178 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Maintenance Storage Bay (', 'Maintenance Storage Bay₄ (') WHERE SDComponentID = 76178 AND Name LIKE 'Maintenance Storage Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76179 AND Name = 'Small Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₃' WHERE SDComponentID = 76179 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Maintenance Storage Bay (', 'Maintenance Storage Bay₃ (') WHERE SDComponentID = 76179 AND Name LIKE 'Maintenance Storage Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76180 AND Name = 'Tiny Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 76180 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Maintenance Storage Bay (', 'Maintenance Storage Bay₂ (') WHERE SDComponentID = 76180 AND Name LIKE 'Maintenance Storage Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76181 AND Name = 'Fighter Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₁' WHERE SDComponentID = 76181 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Maintenance Storage Bay (', 'Maintenance Storage Bay₁ (') WHERE SDComponentID = 76181 AND Name LIKE 'Maintenance Storage Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 78585 AND Name = 'Troop Transport Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 78585 AND Name = 'Troop Transport Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Bay (', 'Troop Transport Bay₂ (') WHERE SDComponentID = 78585 AND Name LIKE 'Troop Transport Bay (%';

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 78586 AND Name = 'Troop Transport Drop Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || '₂' WHERE SDComponentID = 78586 AND Name = 'Troop Transport Drop Bay';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, 'Troop Transport Drop Bay (', 'Troop Transport Drop Bay₂ (') WHERE SDComponentID = 78586 AND Name LIKE 'Troop Transport Drop Bay (%';

