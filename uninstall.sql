UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 3;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Small' WHERE SDComponentID = 3 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 8;
UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 8 AND Name = 'Crew Quarters';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 479;
UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 479 AND Name = 'Cryogenic Transport';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 600;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 600 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 728;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay - Large' WHERE SDComponentID = 728 AND Name = 'Troop Transport Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12017;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 12017 AND Name = 'ECM-1';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12018;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 12018 AND Name = 'ECM-2';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12019;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 12019 AND Name = 'ECM-3';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12020;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 12020 AND Name = 'ECM-4';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12021;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 12021 AND Name = 'ECM-5';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12022;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-6' WHERE SDComponentID = 12022 AND Name = 'ECM-6';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12023;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-7' WHERE SDComponentID = 12023 AND Name = 'ECM-7';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12024;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-8' WHERE SDComponentID = 12024 AND Name = 'ECM-8';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12025;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-9' WHERE SDComponentID = 12025 AND Name = 'ECM-9';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12026;
UPDATE FCT_ShipDesignComponents SET Name = 'ECM-10' WHERE SDComponentID = 12026 AND Name = 'ECM-10';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12027;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 12027 AND Name = 'ECCM-1';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12028;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 12028 AND Name = 'ECCM-2';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12029;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 12029 AND Name = 'ECCM-3';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12030;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 12030 AND Name = 'ECCM-4';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12031;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 12031 AND Name = 'ECCM-5';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12032;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-6' WHERE SDComponentID = 12032 AND Name = 'ECCM-6';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12033;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-7' WHERE SDComponentID = 12033 AND Name = 'ECCM-7';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12034;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-8' WHERE SDComponentID = 12034 AND Name = 'ECCM-8';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12035;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-9' WHERE SDComponentID = 12035 AND Name = 'ECCM-9';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 12036;
UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-10' WHERE SDComponentID = 12036 AND Name = 'ECCM-10';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12037;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-1' WHERE SDComponentID = 12037 AND Name = 'ECM-1';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12038;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-2' WHERE SDComponentID = 12038 AND Name = 'ECM-2';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12039;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-3' WHERE SDComponentID = 12039 AND Name = 'ECM-3';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12040;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-4' WHERE SDComponentID = 12040 AND Name = 'ECM-4';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12041;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-5' WHERE SDComponentID = 12041 AND Name = 'ECM-5';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12042;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-6' WHERE SDComponentID = 12042 AND Name = 'ECM-6';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12043;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-7' WHERE SDComponentID = 12043 AND Name = 'ECM-7';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12044;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-8' WHERE SDComponentID = 12044 AND Name = 'ECM-8';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12045;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-9' WHERE SDComponentID = 12045 AND Name = 'ECM-9';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12046;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECM-10' WHERE SDComponentID = 12046 AND Name = 'ECM-10';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12047;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-1' WHERE SDComponentID = 12047 AND Name = 'ECCM-1';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12048;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-2' WHERE SDComponentID = 12048 AND Name = 'ECCM-2';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12049;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-3' WHERE SDComponentID = 12049 AND Name = 'ECCM-3';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12050;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-4' WHERE SDComponentID = 12050 AND Name = 'ECCM-4';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12051;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-5' WHERE SDComponentID = 12051 AND Name = 'ECCM-5';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12052;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-6' WHERE SDComponentID = 12052 AND Name = 'ECCM-6';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12053;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-7' WHERE SDComponentID = 12053 AND Name = 'ECCM-7';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12054;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-8' WHERE SDComponentID = 12054 AND Name = 'ECCM-8';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12055;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-9' WHERE SDComponentID = 12055 AND Name = 'ECCM-9';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 12056;
UPDATE FCT_ShipDesignComponents SET Name = 'Compact ECCM-10' WHERE SDComponentID = 12056 AND Name = 'ECCM-10';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 24990;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 180' WHERE SDComponentID = 24990 AND Name = 'Jump Point Stabilisation Module 180days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 24993;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 150' WHERE SDComponentID = 24993 AND Name = 'Jump Point Stabilisation Module 150days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 24994;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 120' WHERE SDComponentID = 24994 AND Name = 'Jump Point Stabilisation Module 120days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₅', '') WHERE SDComponentID = 24995;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 90' WHERE SDComponentID = 24995 AND Name = 'Jump Point Stabilisation Module 90days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₆', '') WHERE SDComponentID = 24996;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 60' WHERE SDComponentID = 24996 AND Name = 'Jump Point Stabilisation Module 60days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₇', '') WHERE SDComponentID = 24997;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 45' WHERE SDComponentID = 24997 AND Name = 'Jump Point Stabilisation Module 45days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₈', '') WHERE SDComponentID = 24998;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 30' WHERE SDComponentID = 24998 AND Name = 'Jump Point Stabilisation Module 30days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₉', '') WHERE SDComponentID = 24999;
UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module - 20' WHERE SDComponentID = 24999 AND Name = 'Jump Point Stabilisation Module 20days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 25147;
UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 25147 AND Name = 'Engineering Spaces';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26265;
UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters - Small' WHERE SDComponentID = 26265 AND Name = 'Crew Quarters';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26266;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage - Small' WHERE SDComponentID = 26266 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26267;
UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces - Small' WHERE SDComponentID = 26267 AND Name = 'Engineering Spaces';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26354;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECM-5' WHERE SDComponentID = 26354 AND Name = 'ECM-5';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26355;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECM-1' WHERE SDComponentID = 26355 AND Name = 'ECM-1';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26357;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECM-2' WHERE SDComponentID = 26357 AND Name = 'ECM-2';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26359;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECM-3' WHERE SDComponentID = 26359 AND Name = 'ECM-3';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26361;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECM-4' WHERE SDComponentID = 26361 AND Name = 'ECM-4';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26363;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECCM-5' WHERE SDComponentID = 26363 AND Name = 'ECCM-5';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26364;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECCM-1' WHERE SDComponentID = 26364 AND Name = 'ECCM-1';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26365;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECCM-2' WHERE SDComponentID = 26365 AND Name = 'ECCM-2';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26366;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECCM-3' WHERE SDComponentID = 26366 AND Name = 'ECCM-3';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 26367;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Craft ECCM-4' WHERE SDComponentID = 26367 AND Name = 'ECCM-4';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 26420;
UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 26420 AND Name = 'Compressed Fuel Storage System';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₅', '') WHERE SDComponentID = 27132;
UPDATE FCT_ShipDesignComponents SET Name = 'Large Maintenance Storage Bay' WHERE SDComponentID = 27132 AND Name = 'Maintenance Storage Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 27133;
UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces - Tiny' WHERE SDComponentID = 27133 AND Name = 'Engineering Spaces';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 27134;
UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces - Fighter' WHERE SDComponentID = 27134 AND Name = 'Engineering Spaces';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 33215;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Jump Point Stabilisation Module' WHERE SDComponentID = 33215 AND Name = 'Jump Point Stabilisation Module 360days';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 33426;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay - Standard' WHERE SDComponentID = 33426 AND Name = 'Troop Transport Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 33433;
UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 33433 AND Name = 'Boat Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 38117;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage - Tiny' WHERE SDComponentID = 38117 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 43528;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Standard' WHERE SDComponentID = 43528 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₅', '') WHERE SDComponentID = 43529;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage - Large' WHERE SDComponentID = 43529 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₇', '') WHERE SDComponentID = 43530;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage - Ultra Large' WHERE SDComponentID = 43530 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₆', '') WHERE SDComponentID = 43531;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage - Very Large' WHERE SDComponentID = 43531 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 43532;
UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport - Emergency' WHERE SDComponentID = 43532 AND Name = 'Cryogenic Transport';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 43533;
UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport - Small' WHERE SDComponentID = 43533 AND Name = 'Cryogenic Transport';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 47485;
UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters - Tiny' WHERE SDComponentID = 47485 AND Name = 'Crew Quarters';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 55437;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Large' WHERE SDComponentID = 55437 AND Name = 'Troop Transport Drop Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 55438;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Standard' WHERE SDComponentID = 55438 AND Name = 'Troop Transport Drop Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 62453;
UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters - Fighter' WHERE SDComponentID = 62453 AND Name = 'Crew Quarters';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 62489;
UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay - Small' WHERE SDComponentID = 62489 AND Name = 'Boat Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 64796;
UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System - Large' WHERE SDComponentID = 64796 AND Name = 'Compressed Fuel Storage System';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 64797;
UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System - Very Large' WHERE SDComponentID = 64797 AND Name = 'Compressed Fuel Storage System';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 65061;
UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System - Small' WHERE SDComponentID = 65061 AND Name = 'Compressed Fuel Storage System';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 65307;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Tiny' WHERE SDComponentID = 65307 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 65454;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay - Standard' WHERE SDComponentID = 65454 AND Name = 'Troop Transport Boarding Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 65848;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay - Small' WHERE SDComponentID = 65848 AND Name = 'Troop Transport Boarding Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 65849;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay - Very Small' WHERE SDComponentID = 65849 AND Name = 'Troop Transport Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 65850;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Very Small' WHERE SDComponentID = 65850 AND Name = 'Troop Transport Drop Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 67058;
UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage - Fighter' WHERE SDComponentID = 67058 AND Name = 'Fuel Storage';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 67059;
UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold - Large' WHERE SDComponentID = 67059 AND Name = 'Cargo Hold';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 67060;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay - Very Small' WHERE SDComponentID = 67060 AND Name = 'Troop Transport Boarding Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₄', '') WHERE SDComponentID = 76178;
UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76178 AND Name = 'Maintenance Storage Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₃', '') WHERE SDComponentID = 76179;
UPDATE FCT_ShipDesignComponents SET Name = 'Small Maintenance Storage Bay' WHERE SDComponentID = 76179 AND Name = 'Maintenance Storage Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 76180;
UPDATE FCT_ShipDesignComponents SET Name = 'Tiny Maintenance Storage Bay' WHERE SDComponentID = 76180 AND Name = 'Maintenance Storage Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₁', '') WHERE SDComponentID = 76181;
UPDATE FCT_ShipDesignComponents SET Name = 'Fighter Maintenance Storage Bay' WHERE SDComponentID = 76181 AND Name = 'Maintenance Storage Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 78585;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay - Small' WHERE SDComponentID = 78585 AND Name = 'Troop Transport Bay';

UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '₂', '') WHERE SDComponentID = 78586;
UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay - Small' WHERE SDComponentID = 78586 AND Name = 'Troop Transport Drop Bay';

