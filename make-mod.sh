#!/bin/sh
dir="${1}"
if [ -z "${dir}" ] || [ ! -d "${dir}" ]; then
    echo must supply valid directory name
    exit 1
fi
cd "${dir}" || exit

function getnames () {
    for prefix in "Fuel Storage" "Compressed Fuel Storage System" \
                  "Engineering Spaces" "Crew Quarters" "Cryogenic Transport" \
                  "Cargo Hold" "Troop Transport Bay" "Boat Bay" \
                  "Troop Transport Boarding Bay" "Troop Transport Drop Bay"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${prefix}' AS NewName,
       (SELECT char
        FROM   (SELECT Size AS value,
                       char(8320 + row_number() OVER (ORDER BY Size)) AS char
                FROM   FCT_ShipDesignComponents
                WHERE  Name LIKE '${prefix}%')
        WHERE  value = Size) AS SortKey
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '${prefix}%';
EOF
    done

    for suffix in "Maintenance Storage Bay"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${suffix}' AS NewName,
       (SELECT char
        FROM   (SELECT Size AS value,
                       char(8320 + row_number() OVER (ORDER BY Size)) AS char
                FROM   FCT_ShipDesignComponents
                WHERE  Name LIKE '%${suffix}')
        WHERE  value = Size) AS SortKey
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${suffix}';
EOF
    done

    for suffix in $(seq --format=ECM-%.0f 1 10) $(seq --format=ECCM-%.0f 1 10); do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${suffix}' AS NewName,
       (SELECT char
        FROM   (SELECT Size AS value,
                       char(8320 + row_number() OVER (ORDER BY Size DESC)) AS char
                FROM   FCT_ShipDesignComponents
                WHERE  Name LIKE '%${suffix}')
        WHERE  value = Size) AS SortKey
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${suffix}';
EOF
    done

    for infix in "Jump Point Stabilisation Module"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${infix}' || ' ' || cast(ComponentValue AS INTEGER) || 'days' AS NewName,
       (SELECT char
        FROM   (SELECT ComponentValue AS value,
                       char(8320 + row_number() OVER (ORDER BY ComponentValue DESC)) AS char
                FROM   FCT_ShipDesignComponents
                WHERE  Name LIKE '%${infix}%')
        WHERE  value = ComponentValue) AS SortKey
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${infix}%';
EOF
    done
}

rm -f -- *.sql.tmp *.sql 2>&-
getnames | sort -t $'\t' -k1n | while IFS=$'\t' read -r id old new sortkey; do
    echo "id='${id}', old='${old}', new=${new}, key=${sortkey}"

    cat <<EOF >> sortkey.sql
UPDATE FCT_ShipDesignComponents SET Name = '${new}' WHERE SDComponentID = ${id} AND Name = '${old}';
UPDATE FCT_ShipDesignComponents SET Name = Name || '${sortkey}' WHERE SDComponentID = ${id} AND Name = '${new}';
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '${new} (', '${new}${sortkey} (') WHERE SDComponentID = ${id} AND Name LIKE '${new} (%';

EOF
    cat <<EOF >> uninstall.sql
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, '${sortkey}', '') WHERE SDComponentID = ${id};
UPDATE FCT_ShipDesignComponents SET Name = '${old}' WHERE SDComponentID = ${id} AND Name = '${new}';

EOF
done
